from django.shortcuts import render, redirect
from accounts.forms import LoginForm
# from django.contrib.auth.models import
from django.contrib.auth import authenticate, login, logout

# Create your views here


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {
            "form": form,
            }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")
